package com.newer.veryweather.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.text.TextUtils;

public class NetUtils {
/*
 * 文件中读取json字符串
 * 仅用于测试
 */
	public static String getXmlFromFile(Context ctxt,int resId){
//		 从raw里面把xml读成字符串
		 InputStream is = ctxt.getResources().openRawResource(resId);
		 String info = null;
		 try {
		 byte[] buf = new byte[is.available()];
		 is.read(buf);
		 info = new String(buf);
		 } catch (IOException e1) {
		 e1.printStackTrace();
		 }
//		 System.out.println(info);
		 return info;
	}
	
/*
 * 下载json方法	
 */
	public static String getXmlFromInternet(String cityId) throws Exception {
//		String uri = "http://192.168.8.200:8080/TestProject/weather.JS"; //本地网络测试字符串
		String uri = "http://m.weather.com.cn/data/" + cityId + ".html";
		System.out.println(uri);
		StringBuilder sb=null;

			URL url = new URL(uri);
			HttpURLConnection conn = (HttpURLConnection) url
					.openConnection();
			conn.setReadTimeout(10 * 1000);
			conn.setRequestMethod("GET");

			InputStreamReader inStream = new InputStreamReader(conn.getInputStream(),"UTF-8");
			BufferedReader buffer = new BufferedReader(inStream);
			sb=new StringBuilder();
			String inputline=null;
			while ((inputline = buffer.readLine()) != null) {
				sb.append(inputline);
			}
			System.out.println("json="+sb.toString());

		
		return sb!=null?sb.toString():null;
	}
	/*
	 * 解析json方法
	 */
	public static Weather parseJson(String jsonStr) throws JSONException {
		Weather weather=null;
		if(jsonStr==null){
			return null;
		}
		
			JSONObject jsonObject1 = new JSONObject(jsonStr);// json第一层
			JSONObject jsonObject = jsonObject1.getJSONObject("weatherinfo");// json第二层

			String city = jsonObject.getString("city");
			String city_en = jsonObject.getString("city_en");
			String date_y = jsonObject.getString("date_y");
			String week = jsonObject.getString("week");
			String cityid = jsonObject.getString("cityid");
			
			String[] temps=new String[6];
			temps[0] = jsonObject.getString("temp1");
			temps[1] = jsonObject.getString("temp2");
			temps[2] = jsonObject.getString("temp3");
			temps[3] = jsonObject.getString("temp4");
			temps[4] = jsonObject.getString("temp5");
			temps[5] = jsonObject.getString("temp6");
			int[][] heightAndLowArray=parseTemps(temps);
			
			String[] weatherCons=new String[6];
			weatherCons[0] = jsonObject.getString("weather1");
			weatherCons[1] = jsonObject.getString("weather2");
			weatherCons[2] = jsonObject.getString("weather3");
			weatherCons[3] = jsonObject.getString("weather4");
			weatherCons[4] = jsonObject.getString("weather5");
			weatherCons[5] = jsonObject.getString("weather6");
			
			int[] imgs=new int[12];
			imgs[0] = jsonObject.getInt("img1");
			imgs[1] = jsonObject.getInt("img2");
			imgs[2] = jsonObject.getInt("img3");
			imgs[3] = jsonObject.getInt("img4");
			imgs[4] = jsonObject.getInt("img5");
			imgs[5] = jsonObject.getInt("img6");
			imgs[6] = jsonObject.getInt("img7");
			imgs[7] = jsonObject.getInt("img8");
			imgs[8] = jsonObject.getInt("img9");
			imgs[9] = jsonObject.getInt("img10");
			imgs[10] = jsonObject.getInt("img11");
			imgs[11] = jsonObject.getInt("img12");
			
			String[] winds=new String[6];
			winds[0] = jsonObject.getString("wind1");
			winds[1] = jsonObject.getString("wind2");
			winds[2] = jsonObject.getString("wind3");
			winds[3] = jsonObject.getString("wind4");
			winds[4] = jsonObject.getString("wind5");
			winds[5] = jsonObject.getString("wind6");

			String fx1 = jsonObject.getString("fx1");
			String fx2 = jsonObject.getString("fx2");
			
			String[] fls=new String[6];
			fls[0] = jsonObject.getString("fl1");
			fls[1] = jsonObject.getString("fl2");
			fls[2] = jsonObject.getString("fl3");
			fls[3] = jsonObject.getString("fl4");
			fls[4] = jsonObject.getString("fl5");
			fls[5] = jsonObject.getString("fl6");

			String index48_d = jsonObject.getString("index48_d");

			weather = new Weather(city, city_en, date_y, week, cityid,
					heightAndLowArray[0],heightAndLowArray[1], weatherCons, imgs, winds,fx1, fx2, fls, index48_d);

		if (weather!=null&&TextUtils.isEmpty(weather.getCity())) {
			return null;
		}
		return weather;
	}
	
	/*
	 * 温度字符串数组分割 例子:29℃~9℃
	 * 二维数组0,i为高温,
	 * 		1,i为低温
	 */
	public static int[][] parseTemps(String[] temps){
		int[][] heightAndLowTemps=new int[2][temps.length];
		for (int i=0,len=temps.length;i<len;i++) {
			String str1=temps[i].split("~")[0].replace("℃", "");
			String str2=temps[i].split("~")[1].replace("℃", "");
			
			heightAndLowTemps[0][i]=Math.max(Integer.parseInt(str2), Integer.parseInt(str1));
			heightAndLowTemps[1][i]=Math.min(Integer.parseInt(str2), Integer.parseInt(str1));
		}
		return heightAndLowTemps;
	}
	
}
