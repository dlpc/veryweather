package com.newer.veryweather.net;

import android.os.Parcel;
import android.os.Parcelable;

//{"weatherinfo":{"city":"大连","city_en":"dalian","date_y":"2012年9月20日","date":"",
//"week":"星期四","fchh":"11","cityid":"101070201",
//"temp1":"25℃~17℃","temp2":"24℃~18℃","temp3":"23℃~17℃","temp4":"20℃~12℃","temp5":"21℃~11℃","temp6":"21℃~12℃",
//"tempF1":"77℉~62.6℉","tempF2":"75.2℉~64.4℉","tempF3":"73.4℉~62.6℉","tempF4":"68℉~53.6℉","tempF5":"69.8℉~51.8℉","tempF6":"69.8℉~53.6℉",
//"weather1":"晴","weather2":"晴转阵雨","weather3":"阵雨","weather4":"阵雨转多云","weather5":"晴","weather6":"晴",
//"img1":"0","img2":"99","img3":"0","img4":"3","img5":"3","img6":"99","img7":"3","img8":"1","img9":"0","img10":"99",
//"img11":"0","img12":"99",
//"img_single":"0","img_title1":"晴","img_title2":"晴","img_title3":"晴","img_title4":"阵雨","img_title5":"阵雨","img_title6":"阵雨","img_title7":"阵雨","img_title8":"多云",
//"img_title9":"晴","img_title10":"晴","img_title11":"晴","img_title12":"晴","img_title_single":"晴",

//"wind1":"西南风4-5级","wind2":"南风4-5级","wind3":"南风转东南风4-5级","wind4":"北风小于3级","wind5":"南风小于3级","wind6":"南风小于3级",
//"fx1":"西南风","fx2":"西南风","fl1":"4-5级","fl2":"4-5级","fl3":"4-5级","fl4":"小于3级","fl5":"小于3级","fl6":"小于3级",
//"index":"热","index_d":"天气较热，建议着短裙、短裤、短套装、T恤等夏季服装。年老体弱者宜着长袖衬衫和单裤。",
//"index48":"热","index48_d":"天气较热，建议着短裙、短裤、短套装、T恤等夏季服装。年老体弱者宜着长袖衬衫和单裤。",
//"index_uv":"强","index48_uv":"中等","index_xc":"不宜","index_tr":"适宜","index_co":"舒适","st1":"24","st2":"16",
//"st3":"23","st4":"14","st5":"20","st6":"13","index_cl":"较适宜","index_ls":"适宜","index_ag":"易发"}}
public class Weather implements Parcelable {
	private String city;// 城市
	private String date_y;// 日期
	private String week;// 星期几
	private String cityid;// 城市id

	private int[] tempsHeight;	// 一个星期的高温度范围
	private int[] tempsLow;	// 一个星期的低温度范围

	private String[] weatherCons;// 一个星期的天气

	private int[] imgs;// 天气图片 一个星期  包括白天晚上

	private String[] winds;//一周 风的级数

	private String fx1;// 一周风向
	private String fx2;
	
	private String[] fls; //一个星期风的级数
	
	private String index48_d;//温馨提示
	

	public Weather() {}


	public Weather(String city, String city_en, String date_y, String week,
			String cityid, int[] tempsHeight,int[]tempsLow,String[] weatherCons, 
			int[] imgs,String[] winds,String fx1,String fx2, 
			String[] fls, String index48_d) {
		this.city = city;
		this.date_y = date_y;
		this.week = week;
		this.cityid = cityid;
		this.tempsHeight=tempsHeight;
		this.tempsLow=tempsLow;
		this.weatherCons=weatherCons;
		this.imgs=imgs;
		this.winds=winds;
		this.fx1 = fx1;
		this.fx2 = fx2;
		this.fls=fls;
		this.index48_d = index48_d;
	}
/*
 * 短信文本,也可用于输出测试
 * 
 */
	@Override
	public String toString() {
		return city+",今日:"+weatherCons[0]+",高温:"+tempsHeight[0]+",低温:"+tempsLow[0]+";明日:"+weatherCons[1]+"高温:"+tempsHeight[1]+",低温:"+tempsLow[1]
				+";"+"后日:"+weatherCons[2]+"高温:"+tempsHeight[2]+",低温:"+tempsLow[2]+"   "+date_y;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getDate_y() {
		return date_y;
	}


	public void setDate_y(String date_y) {
		this.date_y = date_y;
	}


	public String getWeek() {
		return week;
	}


	public void setWeek(String week) {
		this.week = week;
	}

	public String getCityid() {
		return cityid;
	}


	public void setCityid(String cityid) {
		this.cityid = cityid;
	}

	public String getFx1() {
		return fx1;
	}


	public void setFx1(String fx1) {
		this.fx1 = fx1;
	}


	public String getFx2() {
		return fx2;
	}


	public void setFx2(String fx2) {
		this.fx2 = fx2;
	}

	public String getIndex48_d() {
		return index48_d;
	}


	public int[] getTempsHeight() {
		return tempsHeight;
	}


	public void setTempsHeight(int[] tempsHeight) {
		this.tempsHeight = tempsHeight;
	}


	public int[] getTempsLow() {
		return tempsLow;
	}


	public void setTempsLow(int[] tempsLow) {
		this.tempsLow = tempsLow;
	}


	public String[] getWeatherCons() {
		return weatherCons;
	}


	public void setWeatherCons(String[] weatherCons) {
		this.weatherCons = weatherCons;
	}


	public int[] getImgs() {
		return imgs;
	}


	public void setImgs(int[] imgs) {
		this.imgs = imgs;
	}


	public String[] getWinds() {
		return winds;
	}


	public void setWinds(String[] winds) {
		this.winds = winds;
	}


	public String[] getFls() {
		return fls;
	}


	public void setFls(String[] fls) {
		this.fls = fls;
	}


	public void setIndex48_d(String index48_d) {
		this.index48_d = index48_d;
	}

//-------------------Parcel-----------------
	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(city);// 城市
		dest.writeString(date_y);// 日期
		dest.writeString(week);// 星期几
		dest.writeString(cityid);// 城市id

		dest.writeIntArray(tempsHeight);	// 一个星期的高温范围
		dest.writeIntArray(tempsLow);		// 一个星期的低温范围
		
		dest.writeStringArray(weatherCons);// 一个星期的天气
		
		dest.writeIntArray(imgs);// 天气图片 一个星期  包括白天晚上

		dest.writeStringArray(winds);//一周 风的级数

		dest.writeString(fx1);// 一周风向
		dest.writeString(fx2);
		
		dest.writeStringArray(fls);//一个星期风的级数
		
		dest.writeString(index48_d);//温馨提示
		
	}
	
	public Weather(Parcel dest) {
		this.city = dest.readString();// 城市
		this.date_y = dest.readString();// 日期
		this.week = dest.readString();// 星期几
		this.cityid = dest.readString();// 城市id

		this.tempsHeight=new int[6];
		dest.readIntArray(tempsHeight);// 一个星期的温度范围
		this.tempsLow=new int[6];
		dest.readIntArray(tempsLow);
		this.weatherCons=new String[6];
		dest.readStringArray(weatherCons);// 一个星期的天气
		this.imgs=new int[12];
		dest.readIntArray(imgs);// 天气图片 一个星期  包括白天晚上
		this.winds=new String[6];
		dest.readStringArray(winds);//一周 风的级数


		this.fx1 = dest.readString();// 一周风向
		this.fx2 = dest.readString();
		
		this.fls=new String[6];
		dest.readStringArray(fls);//一个星期风的级数
		
		this.index48_d = dest.readString();//温馨提示
	}

	public static final Parcelable.Creator<Weather> CREATOR = new Creator<Weather>() {

		@Override
		public Weather createFromParcel(Parcel source) {
			return new Weather(source);
		}

		@Override
		public Weather[] newArray(int size) {
			return new Weather[size];
		}
		
	};
	//--------------Parcel--------------------

}
