package com.newer.veryweather.Util;

import java.util.List;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.newer.veryweather.R;
import com.newer.veryweather.net.Weather;

public class MapPagerAdapter extends PagerAdapter {
	List<Weather> weatherList=null;
	private LayoutInflater inflater;
	public MapPagerAdapter(Context ctxt,List<Weather> data){
		inflater = (LayoutInflater) ctxt.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		weatherList=data;
	}
	
	@Override
	public Object instantiateItem(ViewGroup container, int position) {
		View layout=(View) inflater.inflate(R.layout.page_layout, null);
		Weather temp=weatherList.get(position);
		layout.setTag(temp);
        TextView tv_tishi_info = (TextView)layout.findViewById(R.id.tv_tishi_info); 		//温馨提示
        tv_tishi_info.setText(temp.getIndex48_d());
        TextView tv_zhuangtai = (TextView)layout.findViewById(R.id.tv_zhuangtai_weather);	//天气状况
        tv_zhuangtai.setText(temp.getWeatherCons()[0]);
        TextView tv_tempH = (TextView)layout.findViewById(R.id.tv_wendu_high);				//高温
        tv_tempH.setText(temp.getTempsHeight()[0]);
        TextView tv_tempL = (TextView)layout.findViewById(R.id.tv_wendu_low);				//低温
        tv_tempL.setText(temp.getTempsLow()[0]);
        TextView tv_wind = (TextView)layout.findViewById(R.id.tv_wind_info);				//风力
        tv_wind.setText(temp.getWinds()[0]+temp.getFls()[0]);
        ImageView iv_zhuangtai = (ImageView)layout.findViewById(R.id.iv_zhuangtai_show);	//天气图标
        iv_zhuangtai.setImageResource(Tools.getPicByIndex(temp.getImgs()[0]));
        TextView tv_time = (TextView)layout.findViewById(R.id.tv_time_show);					//更新时间
        View ll_page=layout.findViewById(R.id.ll_page);
        ll_page.setBackgroundResource(Tools.getBackgroundByIndex(temp.getImgs()[0]));
        return layout;
	}

	@Override
	public int getCount() {
		return weatherList.size();
	}
/*
 * 通过城市名称对比
 * 
 */
	@Override
	public boolean isViewFromObject(View arg0, Object arg1) {
		return ((Weather)arg0.getTag()).getCity().equals(arg1);
	}

}
