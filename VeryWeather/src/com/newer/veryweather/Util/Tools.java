package com.newer.veryweather.Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.newer.veryweather.R;

public class Tools {

	/*
	 * 时钟数字 根据数字选取响应的数字图片
	 */
	public static int getPicByNum(int num) {
		switch (num) {
		case 0:
			return R.drawable.number_0_tahoma;
		case 1:
			return R.drawable.number_1_tahoma;
		case 2:
			return R.drawable.number_2_tahoma;
		case 3:
			return R.drawable.number_3_tahoma;
		case 4:
			return R.drawable.number_4_tahoma;
		case 5:
			return R.drawable.number_5_tahoma;
		case 6:
			return R.drawable.number_6_tahoma;
		case 7:
			return R.drawable.number_7_tahoma;
		case 8:
			return R.drawable.number_8_tahoma;
		case 9:
			return R.drawable.number_9_tahoma;
		default:
			break;
		}
		return R.drawable.number_0_tahoma;
	}

	/*
	 * 通过索引获取天气图片
	 */
	public static int getPicByIndex(int index) {
		switch (index) {
		case 0:
			return R.drawable.w0;
		case 1:
			return R.drawable.w1;
		case 2:
			return R.drawable.w2;
		case 3:
			return R.drawable.w3;
		case 4:
			return R.drawable.w4;
		case 5:
			return R.drawable.w5;
		case 6:
			return R.drawable.w6;
		case 7:
			return R.drawable.w7;
		case 8:
			return R.drawable.w8;
		case 9:
		case 10:
		case 11:
		case 12:
		case 22:
		case 23:
		case 24:
		case 25:
			return R.drawable.w12;
		case 13:
		case 14:
			return R.drawable.w14;
		case 15:
		case 16:
		case 17:
		case 26:
		case 27:
		case 28:
			return R.drawable.w17;
		case 18:
			return R.drawable.w18;
		case 19:
			return R.drawable.w19;
		case 20:
		case 31:
			return R.drawable.w20;
		case 21:
			return R.drawable.w21;
		case 29:
		case 30:
			return R.drawable.w30;

		default:
			break;
		}
		return R.drawable.w0;
	}

	/*
	 * 通过天气情况获得背景图片
	 */

	public static int getBackgroundByIndex(int index) {

		switch (index) {
		// 0=晴1=多云2=阴
		case 0:
		case 1:
		case 2:
			return R.drawable.weather_fina_cloudy;

			// 7=小雨8=中雨9=大雨21=小雨-中雨22=中雨-大雨
		case 7:
		case 8:
		case 9:
		case 21:
		case 22:
			return R.drawable.weather_spint_and_hale_rain;

			// 10=暴雨11=大暴雨12=特大暴雨23=大雨-暴雨
		case 10:
		case 11:
		case 12:
		case 23:
			return R.drawable.weather_fog;

			// 24=暴雨-大暴雨25=大暴雨-特大暴雨3=阵雨4=雷阵雨

		case 3:
		case 4:
		case 24:
		case 25:

			return R.drawable.weather_rainstorm;

			// 16=大雪14=小雪15=中雪26=小雪-中雪

		case 14:
		case 15:
		case 16:
		case 26:

			return R.drawable.weather_snow;

			// 5=雷阵雨伴有冰雹6=雨夹雪19=冻雨18=雾

		case 5:
		case 6:
		case 18:
		case 19:
			return R.drawable.weather_snow_aaa;

			// 29=浮尘30=扬沙31=强沙尘暴20=沙尘暴

		case 20:
		case 29:
		case 30:
		case 31:
			return R.drawable.bg_overcast;

			// 17=暴雪13=阵雪27=中雪-大雪28=大雪-暴雪

		case 13:
		case 17:
		case 27:
		case 28:
			return R.drawable.weather_sleet;

		default:
			return R.drawable.weather_sunny_night;
		}

	}
	
/*
 *将2012年9月20转化为calander 
 */
	private static Calendar FormatDate(String date){
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy年MM月dd");
		
		Calendar cal=Calendar.getInstance();
		try {
			Date mydate=sdf.parse(date);
			cal.setTime(mydate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return cal;
	}
/*
 * 将cal转换为09/12(月/日)字符串	
 */
	private static String getDateStr(Calendar cal){
		return (cal.get(Calendar.MONTH)+1)+"/"+cal.get(Calendar.DAY_OF_MONTH);
	}
	

/*
 * 获得09/12(月/日)字符串数组	
 */
	public static String[] getDateArray(String date){
		 String[] dateArray=new String[6];
		 Calendar cal=FormatDate(date);
		for (int i = 0; i < dateArray.length; i++) {
			dateArray[i]=getDateStr(cal);
			cal.add(Calendar.DAY_OF_MONTH, 1);
		}
		return dateArray;
	}
	/*
	 * 获得星期数组
	 */
	public static String[] getWeekDayArray(String date){
		String[] weekday=new String[]{"","周日","周一","周二","周三","周四","周五","周六"};
		 String[] weekdayArray=new String[6];
		 Calendar cal=FormatDate(date);
		for (int i = 0; i < weekdayArray.length; i++) {
			weekdayArray[i]=weekday[cal.get(Calendar.DAY_OF_WEEK)];
			cal.add(Calendar.DAY_OF_MONTH, 1);
		}
		return weekdayArray;		
	}
	/*
	 * 根据天气编号返回简短天气描述(1-2字)
	 * 0=晴1=多云2=阴3=阵雨4=雷阵雨5=雷阵雨伴有冰雹6=雨夹雪7=小雨8=中雨9=大雨
	10=暴雨11=大暴雨12=特大暴雨13=阵雪14=小雪15=中雪16=大雪17=暴雪18=雾19=冻雨
	20=沙尘暴21=小雨-中雨22=中雨-大雨23=大雨-暴雨24=暴雨-大暴雨25=大暴雨-特大暴雨
	26=小雪-中雪27=中雪-大雪28=大雪-暴雪29=浮尘30=扬沙31=强沙尘暴
	 * 
	 */
	public static String getShortWeather(int weatherCode){
		switch (weatherCode) {
		// 0=晴1=多云2=阴
		case 0:
			return "晴";
		case 1:
			return "多云";
		case 2:
			return "阴";

			// 7=小雨8=中雨9=大雨21=小雨-中雨22=中雨-大雨
		case 7:
			return "小雨";
		case 21:
		case 8:
			return "中雨";
		case 22:
		case 9:
			return "大雨";
			// 10=暴雨11=大暴雨12=特大暴雨23=大雨-暴雨
		case 10:
		case 11:
		case 12:
		case 23:
		case 24:
		case 25:
			return "暴雨";

			// 24=暴雨-大暴雨25=大暴雨-特大暴雨3=阵雨4=雷阵雨

		case 3:
		case 4:
			return "阵雨";


			// 16=大雪14=小雪15=中雪26=小雪-中雪

		case 14:
			return "小雪";
		case 26:
		case 15:
			return "中雪";
		case 27:	
		case 16:
			return "大雪";
			// 5=雷阵雨伴有冰雹6=雨夹雪19=冻雨18=雾

		case 5:
			return "冰雹";
		case 6:
		case 19:
			return "冻雨";
		case 18:
			return "雾";

			// 29=浮尘30=扬沙31=强沙尘暴20=沙尘暴
		case 31:
		case 20:
			return "沙暴";
		case 29:
			return "浮尘";
		case 30:
			return "扬沙";
		
			// 17=暴雪13=阵雪27=中雪-大雪28=大雪-暴雪
		case 13:
			return "阵雪";
		case 28:
		case 17:
			return "暴雪";
		default:
			return "晴";
		}
	}

}
