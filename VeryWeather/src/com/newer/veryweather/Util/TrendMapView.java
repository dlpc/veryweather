package com.newer.veryweather.Util;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.view.View;

import com.newer.veryweather.net.Weather;
/*
 *	高低温曲线 
 * 
 * 
 * 
 */

public class TrendMapView extends View {

	Weather weather = null;

//	上下左右的边距
	private int paddingRight=30;
	private int paddingLeft=30;
	private int paddingTop=140;
	private int paddingBottom=130;
	private int h;//工作画图区高度,天气图片和日期,天气文字未算在内
	private int w;//工作画图区宽度,星期文字未算在内
	private float disX;//X轴单位宽度
	private float disY;//Y轴单位宽度
	private int maxTemp;
	private ArrayList<Point> heightPoints;
	private ArrayList<Point> lowPoints;
	private Context ctxt;

	private int screenH;

	private int screenW;

	private Matrix matrix;

	private Paint paint;
	
	
	public TrendMapView(Context context) {
		super(context);
		ctxt = context;
		screenH = Resources.getSystem().getDisplayMetrics().heightPixels;
		screenW = Resources.getSystem().getDisplayMetrics().widthPixels;
		System.out.println("屏幕像素:"+screenH+" "+screenW);
		h = screenH-paddingTop-paddingBottom;
		w = screenW-paddingLeft-paddingRight;
		System.out.println("工作区像素:"+h+" "+w);
		matrix = new Matrix();
		matrix.postScale(0.3f, 0.3f);
		paint = new Paint();
		
	}
	
	public void onDraw(Canvas canvas) {
		System.out.println("TrendMapView-->onDraw");
		super.onDraw(canvas);
		
		if (weather == null) {
			return;
		}
		
		
		paint.setAntiAlias(true);	//反锯齿
		paint.setColor(Color.BLUE);
		paint.setStyle(Paint.Style.STROKE);
		
		paint.setStrokeWidth(4f);
		canvas.drawPath(getPathFromList(heightPoints), paint);//高温线
		canvas.drawPath(getPathFromList(lowPoints), paint);//低温线
		
		
		for (int i=0,len=heightPoints.size();i<len;i++) {
			Point point=heightPoints.get(i);
			
			paint.setStrokeWidth(6f);
			paint.setColor(Color.RED);
			canvas.drawPoint(point.x, point.y, paint);//温度点
			
			paint.setStrokeWidth(1f);
			paint.setColor(Color.BLACK);
			canvas.drawText(weather.getTempsHeight()[i]+"℃", point.x-8, point.y+20, paint);//温度文字
			canvas.drawText(Tools.getDateArray(weather.getDate_y())[i], point.x-8, paddingTop-60, paint);//日期文字
			canvas.drawText(Tools.getShortWeather(weather.getImgs()[i]), point.x-8, paddingTop-40, paint);//短天气文字
			
			Bitmap bmp=BitmapFactory.decodeResource(ctxt.getResources(),Tools.getPicByIndex(weather.getImgs()[i]));
			Bitmap dstbmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(),   
	                matrix, true);
			canvas.drawBitmap(dstbmp, point.x-25, point.y-40, paint);
			
		}
		for (int i=0,len=lowPoints.size();i<len;i++) {
			Point point=lowPoints.get(i);
			canvas.drawPoint(point.x, point.y, paint);
			
			paint.setStrokeWidth(6f);
			paint.setColor(Color.RED);
			canvas.drawPoint(point.x, point.y, paint);//温度点
			
			paint.setStrokeWidth(1f);
			paint.setColor(Color.BLACK);
			canvas.drawText(weather.getTempsLow()[i]+"℃", point.x-8, point.y+20, paint);//温度文字
			canvas.drawText(Tools.getWeekDayArray(weather.getDate_y())[i], point.x-8, screenH-paddingBottom+30, paint);//星期文字
			
			Bitmap bmp=BitmapFactory.decodeResource(ctxt.getResources(),Tools.getPicByIndex(weather.getImgs()[i]));
			Bitmap dstbmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(),   
	                matrix, true);
			canvas.drawBitmap(dstbmp, point.x-25, point.y-40, paint);
		}

	}

	
	public void setWeather(Weather weather){
		this.weather=weather;
		if(weather==null){
			return;
		}
		int days=5;	//有6天,
		disX = (float) (w/days);	//X轴单位长度
		System.out.println("X轴间距:"+disX);
		maxTemp = maxInArray(weather.getTempsHeight());
		int minTemp=minInArray(weather.getTempsLow());	//最低温度
		disY = h/(maxTemp-minTemp);	//Y轴单位长度
		
		heightPoints = transToPoint(weather.getTempsHeight());
		lowPoints = transToPoint(weather.getTempsLow());
		
	}
	
	/*
	 * 温度位置坐标转换
	 */
	private Point toPoint(int position,int temp){
		Point point=new Point();
		point.x=(int)(position*disX)+paddingLeft;		//paddingLeft为工作区原点X值
		point.y=(int)((maxTemp-temp)*disY)+paddingTop;	//paddingRight为工作区原点Y值
		return point;
	}
	
	/*
	 * 将温度数组转换成实际像素点集合
	 */
	private ArrayList<Point> transToPoint(int[] temps){
		ArrayList<Point> pointList=new ArrayList<Point>();
		for (int i = 0,len=temps.length; i < len; i++) {
			Point tempPoint=toPoint(i,temps[i]);
			pointList.add(tempPoint);
		}
		return pointList;
	}
	
	/*
	 * 使用集合中的点转换成路径Path
	 * 
	 */
	private Path getPathFromList(List<Point> list){
		Path path=null;
		if(list!=null){
			path=new Path();
			for (int i=0,len=list.size();i<len;i++) {
				Point point=list.get(i);
				if(i==0){
					path.moveTo(point.x, point.y);
				}else{
					path.lineTo(point.x, point.y);
				}
			}
		}
		return path;
	}
	
	/*
	 * 取得数组中最大的值	
	 */
		private int maxInArray(int[] temps){
			int maxNum=temps[0];
			for (int f : temps) {
				maxNum=maxNum>f?maxNum:f;
			}
			return maxNum;
		}
	/*
	 * 取得数组中最小的值	
	 */
		private int minInArray(int[] temps){
			int minNum=temps[0];
			for (int f : temps) {
				minNum=minNum<f?minNum:f;
			}
			return minNum;
		}
	
}

	
