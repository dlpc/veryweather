package com.newer.veryweather.ui;

import java.util.Calendar;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.provider.AlarmClock;
import android.widget.RemoteViews;

import com.newer.veryweather.R;
import com.newer.veryweather.Util.Tools;
import com.newer.veryweather.net.Weather;
import com.newer.veryweather.service.WeatherService;

public class WeatherWidget extends AppWidgetProvider {
	public static String BROADCAST_ACTION="com.newer.veryweather.ui.weatherwidget";
	private static Calendar cal=null;

	@Override
	public void onReceive(Context context, Intent intent) {
		if(BROADCAST_ACTION.equals(intent.getAction())){
			Weather weather=(Weather) intent.getParcelableExtra("weather");
			updataWeather(context,weather);
		}else{
			super.onReceive(context, intent);
		}
	}
	
	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		
		//启动服务,服务启动后会立即调用更新时间和天气,
		Intent serviceIntent=new Intent(WeatherService.ACTION);
		serviceIntent.putExtra("command", 1);
		context.startService(serviceIntent);
		
		RemoteViews rViews=new RemoteViews(context.getPackageName(), R.layout.app_widget);
		//天气图标点击事件,启动主界面
		Intent weatherIntent=new Intent(context, WeatherTabActivity.class);
		PendingIntent weatherPendingIntent=PendingIntent.getActivity(context, 0, weatherIntent, 0);
		rViews.setOnClickPendingIntent(R.id.forecastImage, weatherPendingIntent);
		
//		点击小时,启动系统闹钟设置界面
		Intent timeIntent=new Intent(AlarmClock.ACTION_SET_ALARM);
		PendingIntent timePendingIntent=PendingIntent.getActivity(context, 0, timeIntent, 0);
		rViews.setOnClickPendingIntent(R.id.hour01Image, timePendingIntent);
		rViews.setOnClickPendingIntent(R.id.hour02Image, timePendingIntent);
		
		appWidgetManager.updateAppWidget(appWidgetIds, rViews);
		
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		updateTime(context);
	}

	public static void updateTime(Context ctxt){
		RemoteViews rViews=new RemoteViews(ctxt.getPackageName(), R.layout.app_widget);
		if(cal==null){
			cal=Calendar.getInstance();
		}
		cal.setTimeInMillis(System.currentTimeMillis());
		int min=cal.get(Calendar.MINUTE);
		int hour=cal.get(Calendar.HOUR_OF_DAY);
		int hour1=hour/10;
		int hour2=hour%10;
		int min1=min/10;
		int min2=min%10;
		rViews.setImageViewResource(R.id.hour01Image, Tools.getPicByNum(hour1));//小时的十位
		rViews.setImageViewResource(R.id.hour02Image, Tools.getPicByNum(hour2));//小时的个位
		rViews.setImageViewResource(R.id.minute01Image, Tools.getPicByNum(min1));//分钟的十位
		rViews.setImageViewResource(R.id.minute02Image, Tools.getPicByNum(min2));//分钟的个位
		AppWidgetManager appWidgetManager=AppWidgetManager.getInstance(ctxt);
		ComponentName componentName=new ComponentName(ctxt, WeatherWidget.class);
		appWidgetManager.updateAppWidget(componentName, rViews);
	}
/*
 * 使用天气更新的静态方法	
 */
	public static void updataWeather(Context ctxt,Weather weather){
		RemoteViews rViews=new RemoteViews(ctxt.getPackageName(), R.layout.app_widget);
		if(rViews==null||weather==null){
			return;
		}
		String weekday="周"+weather.getWeek().charAt(weather.getWeek().length()-1);
		rViews.setTextViewText(R.id.dayText, weekday);//设置星期几
		rViews.setTextViewText(R.id.dateText, weather.getDate_y());//设置日期
		rViews.setTextViewText(R.id.cityText, weather.getCity());//设置城市
		float height=weather.getTempsHeight()[0];
		float low=weather.getTempsLow()[0];
		double midderTemp=(height+low)/2.0; 
		rViews.setTextViewText(R.id.tempCText, weather.getWeatherCons()[0]+" "+midderTemp+"");//设置温度
		rViews.setTextViewText(R.id.hightText, "H "+height+"℃");//设置高温
		rViews.setTextViewText(R.id.lowText, "L "+low+"℃");//设置低温
		rViews.setImageViewResource(R.id.forecastImage, Tools.getPicByIndex(weather.getImgs()[0]));//设置天气图片
		AppWidgetManager appWidgetManager=AppWidgetManager.getInstance(ctxt);
		ComponentName componentName=new ComponentName(ctxt, WeatherWidget.class);
		appWidgetManager.updateAppWidget(componentName, rViews);
	}
	
}
