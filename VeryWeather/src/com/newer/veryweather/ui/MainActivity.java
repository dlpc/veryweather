package com.newer.veryweather.ui;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.newer.veryweather.R;
import com.newer.veryweather.Util.Tools;
import com.newer.veryweather.net.Weather;
import com.newer.veryweather.service.WeatherService;

public class MainActivity extends Activity{
	
	private Intent intent = null;
	private TextView tv_tishi_info;
	private TextView tv_city_name;
	private TextView tv_zhuangtai;
	private TextView tv_tempH;
	private TextView tv_tempL;
	private TextView tv_wind;
	private WeatherBrodcast weatherReciver;
	private ImageView iv_zhuangtai;
	private TextView tv_time;
	private View fl_parent;
	private ImageView iv_load;
	private ViewPager vpager;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        //实例化组件
        initView();
        
//		 注册广播接收器
        weatherReciver = new WeatherBrodcast();
        IntentFilter filter=new IntentFilter(WeatherWidget.BROADCAST_ACTION);
        registerReceiver(weatherReciver, filter);
        
        
        //启动服务
        Intent serviceIntent=new Intent(WeatherService.ACTION);
        serviceIntent.putExtra("command", 1);//配置命令,立即将天气通过广播发送过来
        startService(serviceIntent);
    }
    
    private void initView(){
//        tv_tishi_info = (TextView) findViewById(R.id.tv_tishi_info); 		//温馨提示
        tv_city_name = (TextView) findViewById(R.id.tv_city_name);			//城市名称
//        tv_zhuangtai = (TextView) findViewById(R.id.tv_zhuangtai_weather);	//天气状况
//        tv_tempH = (TextView) findViewById(R.id.tv_wendu_high);				//高温
//        tv_tempL = (TextView) findViewById(R.id.tv_wendu_low);				//低温
//        tv_wind = (TextView) findViewById(R.id.tv_wind_info);				//风力
//        iv_zhuangtai = (ImageView) findViewById(R.id.iv_zhuangtai_show);	//天气图标
//        tv_time=(TextView)findViewById(R.id.tv_time_show);					//更新时间
//        fl_parent = findViewById(R.id.fl_parent);							//背景
        iv_load = (ImageView) findViewById(R.id.iv_update_male);
        vpager = (ViewPager) findViewById(R.id.vPager);
        		
    }
    /*
     * 分享图片点击事件
     * 短信分享
     */
    public void doShare(View v){
    	Intent it = new Intent(Intent.ACTION_VIEW);  
    	Weather currentWeather=WeatherService.weather1;
    	if(currentWeather!=null){
    		it.putExtra("sms_body", currentWeather.toString());   
    		it.setType("vnd.android-dir/mms-sms");   
    		startActivity(it);  
    	}else{
    		Toast.makeText(this, "当前天气不可用!请联网更新", Toast.LENGTH_SHORT).show();
    	}
    	
    }
    
    /*
     * 添加城市 图片按钮事件
     */
	public void doAddCity(View v) {
		intent = new Intent(MainActivity.this, ManageActivity.class);
    	startActivity(intent);
	}
	
	@Override
	protected void onDestroy() {
//		unregisterReceiver(weatherReciver);
		super.onDestroy();
	}

///*
// * 使用天气更新界面
// */
//	public void updataView(Weather weather,int errCode){
//		if(weather==null){
//			//错误警告
//			if(errCode>0){
//				Toast.makeText(this, "网络错误,请检查网络连接是否正常!", Toast.LENGTH_LONG).show();
//			}
//			return;
//		}
//		tv_tishi_info.setText(weather.getIndex48_d());//提示
//		tv_city_name.setText(weather.getCity());
//		tv_zhuangtai.setText(weather.getWeatherCons()[0]);
//		float low=weather.getTempsLow()[0];
//		float height=weather.getTempsHeight()[0];
//		tv_tempH.setText(height+"℃");
//		tv_tempL.setText(low+"℃");
//		tv_wind.setText(weather.getWinds()[0]);
//		iv_zhuangtai.setImageResource(Tools.getPicByIndex(weather.getImgs()[0]));
//		tv_time.setText(weather.getDate_y());
//		//设置背景
//		fl_parent.setBackgroundResource(Tools.getBackgroundByIndex(weather.getImgs()[0]));
//		
//	}
	@Override
	protected void onResume() {
		
		iv_load.startAnimation(AnimationUtils.loadAnimation(this, R.anim.load_anima));
		super.onResume();
	}
/*
 * 手动更新图片按钮事件	
 */
	public void doUpdateWeather(View v){
		iv_load.startAnimation(AnimationUtils.loadAnimation(this, R.anim.load_anima));
        Intent serviceIntent=new Intent(WeatherService.ACTION);
        serviceIntent.putExtra("command", 2);//配置命令,立即更新
        startService(serviceIntent);		
	}
	class WeatherBrodcast extends BroadcastReceiver{
		@Override
		public void onReceive(Context context, Intent intent) {
//			updataView((Weather)intent.getParcelableExtra("weather"),intent.getIntExtra("err", 0));
		}
	}
}