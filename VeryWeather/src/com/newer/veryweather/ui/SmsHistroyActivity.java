package com.newer.veryweather.ui;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.newer.veryweather.R;
import com.newer.veryweather.db.SMSDAO;

public class SmsHistroyActivity extends Activity {
	
	private SMSDAO sDao;
	private ListView listView;
	private Cursor cursor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.smshistroy_activity);
		sDao = new SMSDAO(this);
		sDao.OpenDataBese();
		listView = (ListView) findViewById(R.id.sms_histroy_listView);
		cursor = sDao.selectAllSMS();
		SimpleCursorAdapter sca=new SimpleCursorAdapter(this, 
				R.layout.sms_histroy_list_item, 
				cursor, new String[]{"phone","content","date","_id"},
				new int[]{R.id.sms_histroy_list_item_phone,
				R.id.sms_histroy_list_item_body,
				R.id.sms_histroy_list_item_date});
		listView.setAdapter(sca);
	}
	
	@Override
	protected void onStop() {
		cursor.close();
		sDao.closeDataBese();
		super.onStop();
	}
	
}
