package com.newer.veryweather.ui;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.newer.veryweather.R;
import com.newer.veryweather.db.CityDAO;

public class AddCityActivity extends Activity implements OnItemClickListener {
	protected static final int QUERY_MESSAGE = 100;
	private AutoCompleteTextView editTextInfo;
	private ArrayAdapter<String> adapter;
	private CityDAO cityDAO;
	private List<String> hotCity;
	private GridView gridViewAddCity;
	private Cursor cursor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.addcity_activity);
		
		initView();
	}

	// select all city from db
	public void initView() {
		cityDAO = new CityDAO(this);
		editTextInfo = (AutoCompleteTextView) findViewById(R.id.editText_infos);
		ArrayList<String> allcitys=cityDAO.getAllCityName();
		ArrayAdapter<String> autoCompleteAdapter=new ArrayAdapter<String>(this, R.layout.auto_complete_item,allcitys);
		editTextInfo.setAdapter(autoCompleteAdapter);
		editTextInfo.setOnItemClickListener(this);
		gridViewAddCity = (GridView) findViewById(R.id.gridView_addcity);
		
		hotCity = cityDAO.getHotCity();

		adapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, hotCity);
		gridViewAddCity.setAdapter(adapter);
		gridViewAddCity.setOnItemClickListener(this);
	}

	public void onItemClick(AdapterView<?> arg0, View view, int arg2, long arg3) {
		// 把所以城市以，拼接起来
		String selectCity = ((TextView) view).getText().toString();// 选中的城市
		insertCity(selectCity);

	}

	private void insertCity(String cityName) {
		SharedPreferences sharedPreferences = getSharedPreferences(
				"weather_pref", MODE_PRIVATE);
		String old = sharedPreferences.getString("cityname", "");
		if (old.contains(cityName)) {
			Toast.makeText(this, "所选城市已存在!", Toast.LENGTH_SHORT).show();
			this.finish();
			return;
		}
		String newValue = "";
		if (TextUtils.isEmpty(old)) {
			newValue = cityName;
		} else {
			newValue = old + "," + cityName;
		}
		SharedPreferences.Editor editor = sharedPreferences.edit();
		editor.putString("cityname", newValue.trim());
		editor.commit();
//		startActivity(new Intent(AddCityActivity.this, ManageActivity.class));
		this.finish();
	}

	// 通过输入城市名字添加城市
//	public void addOk(View v) {
//		String info = editTextInfo.getText().toString().trim();
//		City city=cityDAO.findCityByName(info);
//		if(city==null){
//			Toast.makeText(AddCityActivity.this, "没有这个城市.", Toast.LENGTH_SHORT).show();
//			editTextInfo.setText("");
//		}else{
//			insertCity(info);
//		}
//		
//	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (cursor != null) {
			cursor.close();
		}
		if (cityDAO != null) {
			cityDAO.closeDB();
		}
		
		
	}

}
