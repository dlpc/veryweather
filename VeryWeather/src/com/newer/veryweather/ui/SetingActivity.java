package com.newer.veryweather.ui;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.text.TextUtils;
import android.widget.Toast;

import com.newer.veryweather.R;
import com.newer.veryweather.service.WeatherService;

public class SetingActivity extends PreferenceActivity implements OnPreferenceClickListener, OnPreferenceChangeListener {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getPreferenceManager().setSharedPreferencesName("weather_pref");
		addPreferencesFromResource(R.xml.setting_pref);
		Preference pref=findPreference("about");
		pref.setOnPreferenceClickListener(this);
		EditTextPreference timePref=(EditTextPreference) findPreference("updata_interval");
		timePref.setSummary("更新时间间隔:"+timePref.getText()+"小时");
		timePref.setOnPreferenceChangeListener(this);
		Preference  hisPref=findPreference("histroy");
		hisPref.setOnPreferenceClickListener(new OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				startActivity(new Intent(SetingActivity.this, SmsHistroyActivity.class));
				return false;
			}
		});
	}
	
	
/*
 * 设置好离开后让服务加载最新的配置	
 * 
 */
	
	@Override
	protected void onPause() {
		System.out.println("SetingActivity-->onPause");
		Intent reloadSettingIntent=new Intent(WeatherService.ACTION);
		reloadSettingIntent.putExtra("command", 2);
		startService(reloadSettingIntent);
		super.onPause();
	}

/*
 * 属性单击事件
 * 
 */
	@Override
	public boolean onPreferenceClick(Preference preference) {
		AlertDialog.Builder builder=new AlertDialog.Builder(this);
		builder.setIcon(R.drawable.app_icon)
				.setTitle(R.string.app_name)
				.setMessage("Version:1.0\nCopyRight 2012")
				.setPositiveButton(android.R.string.ok, null).show();
		
		return false;
	}
/*
 * 更改监听器
 * 
 */

	@Override
	public boolean onPreferenceChange(Preference preference, Object newValue) {
		if(isGoodData(newValue)){
			preference.setSummary("更新时间间隔:"+newValue+"小时");
			return true;	//返回true,更新值
		}
		Toast.makeText(this, "请正确输入!", Toast.LENGTH_SHORT).show();
		return false;		//返回false,保留原值
	}
	
	public boolean isGoodData(Object obj){
		if(!TextUtils.isDigitsOnly(obj.toString())){
			return false;
		}
		int temp=Integer.parseInt(obj.toString());
		if(temp>23||temp<1){
			return false;
		}
		return true;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
//
//	private ListView set_list = null;
//	private SimpleAdapter adapter = null;
//
//	private List<Map<String, Object>> listViewItem = null;
//	private Map<String, Object> item = null;
//
//	public void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.setting);
//
//		set_list = (ListView) findViewById(R.id.set_list);
//		//获得设置项集合
//		listViewItem=initSettingItems();
//		
//		adapter = new SimpleAdapter(this, listViewItem, R.layout.set_items,
//				new String[] {"icon","title"}, 
//				new int[] {R.id.set_list_img, R.id.set_lsit_title});
//		set_list.setAdapter(adapter);
//	}
///*
// * 生产设置项集合
// */
//	private List<Map<String, Object>> initSettingItems() {
//		List<Map<String, Object>> itemList = new ArrayList<Map<String, Object>>();
//
//		item = new HashMap<String, Object>();
//		item.put("icon", R.drawable.setting_share);
//		item.put("title", "天气分享");
//		listViewItem.add(item);
//
//		item = new HashMap<String, Object>();
//		item.put("icon", R.drawable.setting_auto_update);
//		item.put("title", "自动更新");
//		listViewItem.add(item);
//
//		item = new HashMap<String, Object>();
//		item.put("icon", R.drawable.setting_desktop);
//		item.put("title", "桌面小部件");
//		listViewItem.add(item);
//
//		item = new HashMap<String, Object>();
//		item.put("icon", R.drawable.setting_notice);
//		item.put("title", "通知栏天气");
//		listViewItem.add(item);
//		
//		item = new HashMap<String, Object>();
//		item.put("icon", R.drawable.setting_help);
//		item.put("title", "帮助");
//		listViewItem.add(item);
//
//		item = new HashMap<String, Object>();
//		item.put("icon", R.drawable.setting_about);
//		item.put("title", "关于");
//		listViewItem.add(item);
//		return itemList;
//	}
	
	
	
	
	
	
	
	
}
