package com.newer.veryweather.ui;

import android.app.Activity;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.newer.veryweather.R;
import com.newer.veryweather.Util.TrendMapView;
import com.newer.veryweather.net.Weather;
import com.newer.veryweather.service.WeatherService;

public class TrendActivity extends Activity {
	//城市
	private TextView weatherTrendCity;

	private Weather weather;

	private TrendMapView trend;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.trend_activity);
		weatherTrendCity = (TextView) findViewById(R.id.weather_trend_city);
		trend = new TrendMapView(this);
		
		FrameLayout flmain=(FrameLayout) findViewById(R.id.framel);
		flmain.addView(trend);
	}
	
	@Override
	protected void onResume() {
		weather = WeatherService.weather1;
		trend.setWeather(weather);
		if(weather!=null){
			weatherTrendCity.setText(weather.getCity());
		}
		super.onResume();
	}
}
