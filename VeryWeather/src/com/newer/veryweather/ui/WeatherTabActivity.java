package com.newer.veryweather.ui;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.TabHost;
import android.widget.TextView;

import com.newer.veryweather.R;

public class WeatherTabActivity extends TabActivity {
	private TabHost tabHost;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab_activitiy);

		tabHost = getTabHost();
		
		// 天气tab
		tabHost.addTab(buildTabSpec(MainActivity.class, "天气", R.drawable.tab_weather_selector));

		// 趋势tab
		tabHost.addTab(buildTabSpec(TrendActivity.class, "趋势", R.drawable.tab_trend_selector));

		// 设置tab
		tabHost.addTab(buildTabSpec(SetingActivity.class, "设置", R.drawable.tab_setting_selector));
		// 设置第一次显示的tab
		tabHost.setCurrentTab(0);
    }

	// 构造tabspec的函数
	// Class to, tab显示的activity
	// int titleResId, tab显示的标题
	// int picResId tab显示的图标
//	private TabHost.TabSpec buildTabSpec(Class<?> to, int titleResId,
//			int picResId) {
//		String title = getText(titleResId).toString();
//		return buildTabSpec(to,title,picResId);
//	}
	// 构造tabspec的函数
	// Class to, tab显示的activity
	// int titleResId, tab显示的标题
	// int picResId tab显示的图标	
	private TabHost.TabSpec buildTabSpec(Class<?> to, String title,
			int picResId) {
//		Drawable icon = null;
//		if (picResId != 0) {
//			icon = getResources().getDrawable(picResId);
//		}
		// 新建一个tabspec,指定一个tag进行区分,这里标题和tag是同一个字符串
		TabHost.TabSpec spec = tabHost.newTabSpec(title);
		// 设置标题和图标
		TextView tab_title=(TextView) LayoutInflater.from(this).inflate(R.layout.tab_widget, null);
		
		tab_title.setText(title);
		
		tab_title.setCompoundDrawablesWithIntrinsicBounds(0, picResId, 0, 0);

		spec.setIndicator(tab_title);
		// 设置tabspec的内容,这里同个一个intent放入一个单独的Activity
		spec.setContent(new Intent(this, to));
		return spec;
	}
	
	
	
}
