package com.newer.veryweather.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.newer.veryweather.R;
import com.newer.veryweather.db.CityDAO;
import com.newer.veryweather.service.WeatherService;

public class ManageActivity extends Activity implements OnItemClickListener, OnItemLongClickListener {

	private GridView add_gv = null;

	private SharedPreferences sharedPreferences;
	private String name;
	private ArrayAdapter<String> adapter;
	private String[] favoriteCity;

	
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.manage_activity);

		add_gv = (GridView) findViewById(R.id.add_gv);

		sharedPreferences = getSharedPreferences("weather_pref", MODE_PRIVATE);


		add_gv.setOnItemClickListener(this);
		add_gv.setOnItemLongClickListener(this);

	}
	@Override
	protected void onResume() {
		name = sharedPreferences.getString("cityname", "nothing");

		favoriteCity = name.split(",");

		adapter = new ArrayAdapter<String>(this, R.layout.add_items,
				R.id.add_city_name1, favoriteCity);
		add_gv.setAdapter(adapter);
		super.onResume();
	}

	public void addCity(View v) {
		Intent intent = new Intent(ManageActivity.this, AddCityActivity.class);
		startActivity(intent);
	}
	
/*
 *	GridView的点击事件处理方法
 * 
 */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,long id) {
		deleteCityName = ((TextView) view.findViewById(R.id.add_city_name1)).getText().toString();
		if (deleteCityName == "") {
			return ;
		}
		CityDAO cityDao=new CityDAO(this);
		String cityId=cityDao.findCityByName(deleteCityName).getCity_id();
		sharedPreferences.edit().putString("currentCity", cityId).commit();//设为当前城市
		Intent changeCityIntent=new Intent(WeatherService.ACTION);
		changeCityIntent.putExtra("command", 2);//让服务重新加载配置文件
		startService(changeCityIntent);
		cityDao.closeDB();
		finish();
	}

	
	public void deleteCity() {
		name = sharedPreferences.getString("cityname", "");
		if (name == deleteCityName) {
			name = "";
		} else if (name.endsWith(deleteCityName)) {// 最后一个城市是没有逗号结尾的
			name = name.replace("," + deleteCityName, "");
		} else if (name.contains(deleteCityName)) {
			name = name.replace(deleteCityName + ",", "");
		}

		sharedPreferences.edit().putString("cityname", name).commit();

		favoriteCity = name.trim().split(",");
		adapter = new ArrayAdapter<String>(this, R.layout.add_items,
				R.id.add_city_name1, favoriteCity);
		add_gv.setAdapter(adapter);
		Intent changeCityIntent=new Intent(WeatherService.ACTION);
		changeCityIntent.putExtra("command", 2);//让服务重新加载配置文件
		startService(changeCityIntent);
	}
/*
 * GridView的长按事件
 */
	private AlertDialog dialog;
	private String deleteCityName;
	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view,
			int position, long id) {
//		Toast.makeText(this, "长按事件", Toast.LENGTH_SHORT).show();
		deleteCityName = ((TextView) view.findViewById(R.id.add_city_name1)).getText().toString();
		if (deleteCityName == "") {
			return true;//返回true不再触发点击
		}
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("删除城市");
		builder.setMessage("你确定要删除吗？");
		builder.setPositiveButton("确定", new AlertDialog.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				deleteCity();
			}
		});
		builder.setNegativeButton("取消", new AlertDialog.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		dialog = builder.create();
		dialog.show();
		
		return true;//返回true不再触发点击
	}

}
