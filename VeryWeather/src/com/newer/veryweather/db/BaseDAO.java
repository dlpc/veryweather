package com.newer.veryweather.db;



import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class BaseDAO {
	SQLiteDatabase sdb = null;
	DBUtil db = null;
	static String DB_NAME="weather.db";
	
	public BaseDAO(Context ctxt) {
		db = new DBUtil(ctxt, DB_NAME, null, 4);
	}

	public void OpenDataBese() {
			sdb = db.getWritableDatabase();
	}
	public void closeDataBese() {
		if (sdb!=null) {
			sdb.close();
		}
	}
}
