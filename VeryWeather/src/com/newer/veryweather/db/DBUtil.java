package com.newer.veryweather.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DBUtil extends SQLiteOpenHelper {

	String sms_id = "Create  TABLE MAIN.[sms_table](" +
			"_id integer PRIMARY KEY NOT NULL," +
			"content varchar NOT NULL," +
			"phone varchar NOT NULL," +
			"date datetime NOT NULL)";
	String weather_id = "Create  TABLE MAIN.[weather_table](" +
			"_id integer PRIMARY KEY NOT NULL," +
			"date datetime NOT NULL," +
			"city_id integer NOT NULL," +
			"text varchar NOT NULL," +
			"high float NOT NULL," +
			"low float NOT NULL," +
			"humidity float NOT NULL)";
	
	public DBUtil(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(sms_id);
//		db.execSQL(weather_id);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("drop table if exists sms_table ");
		db.execSQL("drop table if exists weather_table");
		onCreate(db);
	}

}
