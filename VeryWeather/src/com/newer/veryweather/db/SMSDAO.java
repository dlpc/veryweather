package com.newer.veryweather.db;

import com.newer.veryweather.entity.SMS;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class SMSDAO extends BaseDAO {

    public SMSDAO(Context context) {
        super(context);
    }

    /*
     * 单条查询
     */
    public SMS findSMSInfoId(String smsId) {
        Cursor smsInfo = super.sdb.query("sms_table", null, "_id=?", new String[] { smsId }, null, null, "desc");
        if (smsInfo.moveToNext()) {
            return SMS.fromCursor(smsInfo);
        }
        return null;

    }

    /*
     * 插入记录
     */
    public boolean insertSMS(SMS sms) {
        long recordId = super.sdb.insert("sms_table", null, sms.toContentValues());
        return recordId > 0;
    }

    /*
     * 更新
     */
    public boolean updateSMS(SMS sms, String smsId) {
        boolean isUpdate = false;
        ContentValues cv = new ContentValues();
        cv.put("content", sms.getContent());
        cv.put("phone", sms.getPhone());
        cv.put("date", sms.getDate());

        int countUpdate = super.sdb.update("sms_table", cv, "_id = ?", new String[] { smsId });
        if (countUpdate > 0) {
            isUpdate = true;
        }
        return isUpdate;
    }

    /*
     * 单条删除
     */
    public boolean deleteSMSById(String smsId) {
        int countDelete = super.sdb.delete("sms_table", "_id = ?", new String[] { smsId });
        return countDelete > 0;
    }

    /*
     * 查询所有
     */
    public Cursor selectAllSMS() {
        return super.sdb.query("sms_table", null, null, null, null, null, "_id DESC");
    }
}
