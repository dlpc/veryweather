package com.newer.veryweather.entity;

import android.database.Cursor;

public class City {
	String _id;
	String city_name;
	String city_id;
	public City(String _id, String city_name, String city_id) {
		super();
		this._id = _id;
		this.city_name = city_name;
		this.city_id = city_id;
	}
	public City() {
	}
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public String getCity_name() {
		return city_name;
	}
	public void setCity_name(String city_name) {
		this.city_name = city_name;
	}
	public String getCity_id() {
		return city_id;
	}
	public void setCity_id(String city_id) {
		this.city_id = city_id;
	}
	@Override
	public String toString() {
		return "City [_id=" + _id + ", city_name=" + city_name + ", city_id="
				+ city_id + "]";
	}
	
	public static City fromCursor(Cursor cityCursor){
	    City city=new City();
	    city.city_id= cityCursor.getString(cityCursor.getColumnIndex("cityid"));
	    city.city_name = cityCursor.getString(cityCursor.getColumnIndex("cityname"));
	    return city;
	}
}
