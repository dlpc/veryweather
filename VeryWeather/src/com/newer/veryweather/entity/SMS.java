package com.newer.veryweather.entity;

import android.content.ContentValues;
import android.database.Cursor;

public class SMS {
	int id;
	String content ;
	String phone;
	String date;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public SMS(int id, String content, String phone, String date) {
		this.id = id;
		this.content = content;
		this.phone = phone;
		this.date = date;
	}

	public SMS() {}
	@Override
	public String toString() {
		return "smsid=" + id + ", content=" + content + ", phone=" + phone
				+ ", date=" + date + "]";
	}
	
	public ContentValues toContentValues(){
        ContentValues cv = new ContentValues();
        cv.put("content",content);
        cv.put("phone",phone);
        cv.put("date",date);
        return cv;
	}
	
	public static SMS fromCursor(Cursor smsInfo){
	    SMS sms=new SMS();
	    sms.id = smsInfo.getInt(smsInfo.getColumnIndex("_id"));
	    sms.content = smsInfo.getString(smsInfo.getColumnIndex("content"));
        sms.phone = smsInfo.getString(smsInfo.getColumnIndex("phone"));
        sms.date = smsInfo.getString(smsInfo.getColumnIndex("date"));
        return sms;
    }
	
}
